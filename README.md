# Para executar

## Instale as dependências necessárias:
- pyknow
- pandas

## Abra o terminal e execute o arquivo `recomendacao_curso.py`, onde os parâmetros serão os argumentos passados no arquivo, por exemplo:

```bash
python3 recomendacao_curso.py --nivel True --tipo Bacharelado --enade 4
```

## Nesse caso, irá procurar pelas universidade que contenham o curso de engenharia com a nota 4 no enade, os tipos são:

- Bacharelado
- Tecnólogo
- Licenciatura
