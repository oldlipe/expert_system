from pyknow import *
import pandas as pd
import argparse

cursos_engenharia = pd.read_csv('../dados/resultado_engenharia_estados_2.csv', sep=';')

cursos_matematica = pd.read_csv('../dados/resultado_enade_matematica.csv', sep=';')

cursos_tecnologia = pd.read_csv('../dados/resultado_enade_tecologia.csv', sep=';')

def consulta_engenharia(nota_enade, estado='SP'):
    print(cursos_engenharia[(cursos_engenharia['Conceito.Enade..Faixa.'] == str(nota_enade)) & (cursos_engenharia['Sigla.da.UF'] == str(estado))]['Nome.da.IES'].unique())

def consulta_matematica(nota_enade):
    print(cursos_matematica[cursos_matematica['Conceito.Enade..Faixa.'] == str(nota_enade)]['Nome.da.IES'].unique())

def consulta_tecnologia(nota_enade):
    print(cursos_tecnologia[cursos_tecnologia['Conceito.Enade..Faixa.'] == str(nota_enade)]['Nome.da.IES'].unique())

class Faculdade(Fact):
    """ Recomendação curso de graduação"""
    pass


class InreferenceEngine(KnowledgeEngine):

    def valor(self, valor):
        self.valor = valor
    
    """
    Regra para nível escolar identificado
    """
    @Rule(Faculdade(nivel=MATCH.nivel),
    TEST(lambda nivel: nivel == True))
    def nivel_escolar(self, nivel):
        self.declare(Fact(nivel_graduacao=True))

    """
    Regra para nível escolar não identificado
    """
    @Rule(Faculdade(nivel=MATCH.nivel),
    TEST(lambda nivel: nivel == False))
    def nao_identificado(self, nivel):
        print("Nível não identificado")
    
    """
    Tipo de graduação:
    - Bacharelado
    - Licenciatura
    - Tecnólogo
    """
    # Tipo bacharelado
    @Rule(Fact(nivel_graduacao=True),
    Faculdade(tipo=MATCH.tipo),
    TEST(lambda tipo: tipo == "Bacharelado"))
    def bacharelado(self, tipo):
        self.declare(Fact(tipo_bacharelado=True))

    # Tipo licenciatura
    @Rule(Fact(nivel_graduacao=True),
    Faculdade(tipo=MATCH.tipo),
    TEST(lambda tipo: tipo == "Licenciatura"))
    def licenciatura(self, tipo):
        self.declare(Fact(tipo_licenciatura=True))
    
    # Tipo tecnologo
    @Rule(Fact(nivel_graduacao=True),
    Faculdade(tipo=MATCH.tipo),
    TEST(lambda tipo: tipo == "Tecnólogo"))
    def tecnologo(self, tipo):
        self.declare(Fact(tipo_tecnologo=True))

    # Curso de engenharia nota enade <= 3
    @Rule(Fact(tipo_bacharelado=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade <= 3))
    def enade_bacharelado_3(self, enade):
        self.declare(Fact(eng_declare_3=True))


    # Curso de engenharia nota enade == 4
    @Rule(Fact(tipo_bacharelado=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 4))
    def enade_bacharelado_4(self, enade):
        self.declare(Fact(eng_declare_4=True))

    # Curso de engenharia nota enade == 5
    @Rule(Fact(tipo_bacharelado=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 5))
    def enade_bacharelado_5(self, enade):
        self.declare(Fact(eng_declare_5=True))

    # Consulta estado do curso
    @Rule(Fact(eng_declare_3=True),
    Faculdade(estado=MATCH.estado))
    def enade_eng_3(self, estado):
        consulta_engenharia(3, estado)

    # Consulta estado do curso
    @Rule(Fact(eng_declare_4=True),
    Faculdade(estado=MATCH.estado))
    def enade_eng_4(self, estado):
        consulta_engenharia(4, estado)

    # Consulta estado do curso
    @Rule(Fact(eng_declare_5=True),
    Faculdade(estado=MATCH.estado))
    def enade_eng_5(self, estado):
        consulta_engenharia(5, estado)

    # Curso de matemática nota enade <= 3
    @Rule(Fact(tipo_licenciatura=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade <= 3))
    def enade_licenciatura_3(self, enade):
        consulta_matematica(enade)

    # Curso de matemática nota enade == 4
    @Rule(Fact(tipo_licenciatura=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 4))
    def enade_licenciatura_4(self, enade):
        consulta_matematica(enade)

    # Curso de matemática nota enade == 5
    @Rule(Fact(tipo_licenciatura=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 5))
    def enade_licenciatura_5(self, enade):
        consulta_matematica(enade)

    # Curso de ADS nota enade <= 3
    @Rule(Fact(tipo_tecnologo=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade <= 3))
    def enade_tecnologo_3(self, enade):
        consulta_tecnologia(enade)

    # Curso de ADS nota enade == 4
    @Rule(Fact(tipo_tecnologo=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 4))
    def enade_tecnologo_4(self, enade):
        consulta_tecnologia(enade)

    # Curso de ADS nota enade == 5
    @Rule(Fact(tipo_tecnologo=True),
    Faculdade(enade=MATCH.enade),
    TEST(lambda enade : enade == 5))
    def enade_tecnologo_5(self, enade):
        consulta_tecnologia(enade)


engine = InreferenceEngine()

engine.reset()

validador = argparse.ArgumentParser(description = 'Sistema especialista de universidades')

validador.add_argument('--nivel', action = 'store', dest = 'nivel',
                           default = True, required = True,
                           help = 'Nivel escolar')


validador.add_argument('--tipo', action = 'store', dest = 'tipo',
                           default = 'Bacharelado', required = True,
                           help = 'Tipo de universidade cursada')

validador.add_argument('--enade', action = 'store', dest = 'enade',
                           default = 5, required = True,
                           help = 'Nota do enade')

validador.add_argument('--estado', action = 'store', dest = 'estado',
                           default = 'SP', required = False,
                           help = 'Estado da universidade')

argumentos = validador.parse_args()

engine.declare(Faculdade(nivel=bool(argumentos.nivel),
                        tipo=str(argumentos.tipo),
                        enade=int(argumentos.enade),
                        estado=str(argumentos.estado)))

engine.run()